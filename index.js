function toggle() {
  var image = document.getElementById('image');
  var button = document.getElementById('button');
  if (image.style.display === 'none') {
    image.style.display = 'block';
    button.innerText = 'Hide me';
  } else {
    image.style.display = 'none';
    button.innerText = 'Show me';
  }
}
